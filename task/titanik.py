import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    pattern = r"(Mr\.|Mrs\.|Miss\.)"
    df["name_title"] = df.Name.str.extract(pattern)
    df_grouped_by_title = df.groupby("name_title")["Age"].agg(number_of_missed_value=lambda x: x.isnull().sum(), median_age=("median")).reset_index().sort_values("name_title", ascending=False)
    df_grouped_by_title["median_age"] = df_grouped_by_title.median_age.round(0).astype(int)
    df_grouped_by_title.iloc[0], df_grouped_by_title.iloc[1] = df_grouped_by_title.iloc[1], df_grouped_by_title.iloc[0]
    return list(df_grouped_by_title.itertuples(index=False, name=None))
